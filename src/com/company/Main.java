package com.company;

import com.company.SemanticStack.CodeGenerator;
import java_cup.runtime.Symbol;

import java.io.*;

public class Main {

    public static void main(String[] args) {
        try {
            File file = new File(args[0]);
            parser p = new parser(new Lexer(new FileReader(file)), file, false);
            Object result = p.parse();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

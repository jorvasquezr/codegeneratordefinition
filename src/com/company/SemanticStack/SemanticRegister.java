package com.company.SemanticStack;

import java.util.HashMap;

public class SemanticRegister {
    private int registerType;
    private HashMap<String, Object> props;
    private HashMap<String, Object> ifSemanticRegister;

    public SemanticRegister(){
        props = new HashMap<>();
    }

    public SemanticRegister(int type){

        registerType = type;
        props = new HashMap<>();
    }
    public void printKeys(){
        Object[] a = props.keySet().toArray();
        for(int i = 0; i<a.length ; i++){
            System.out.print((String)a[i]+" ");
        }
        System.out.println();

    }
    public void printValue(){
        Object[] a = props.values().toArray();
        for(int i = 0; i<a.length ; i++){
            System.out.print((String)a[i]+" ");
        }
        System.out.println();


    }

    public int getRegisterType() {
        return registerType;
    }

    public void setRegisterType(int registerType) {
        this.registerType = registerType;
    }

    public void setProperty(String name, Object value){
        props.put(name, value);
    }

    public Object getProperty(String name){
        return props.get(name);
    }
}

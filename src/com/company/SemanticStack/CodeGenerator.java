package com.company.SemanticStack;

import com.company.PClasses.*;
import com.company.parser;
import java_cup.runtime.Symbol;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class CodeGenerator {

    public PSymbolTable table;
    private Stack<SemanticRegister> stack;
    private boolean hasError = false;
    private StringBuilder fileContent;
    private StringBuilder dataSection;
    private StringBuilder bssSection = new StringBuilder("section .bss\n\treturn:\tresw\t2\n ");
    private StringBuilder textSection;
    private String outputName = "a.asm";
    private com.company.parser parser;
    private boolean isElf32;
    private int ifCounter=0;
    private int whileCounter=0;


    public CodeGenerator(File f, parser s, boolean isElf32){
        this.table = new PSymbolTable();
        this.stack = new Stack<>();
        String fileName = f.getName();
        fileName = fileName.substring(0, fileName.indexOf('.'));
        fileName += ".asm";
        outputName = fileName;
        parser = s;
        this.isElf32 = isElf32;
        if(isElf32){
            fileContent = new StringBuilder("global main \n" +
                    "extern printf\n");
            dataSection =new StringBuilder("section .data\n" +
                    "\treturnMsg:\tdb\t\"El resultado es: %d\", 10 , 0\n");
            textSection = new StringBuilder("section .text\n" +
                    "main:\n");
        } else {
            fileContent = new StringBuilder("global _main \n" +
                    "extern _printf\n");
            dataSection =new StringBuilder("section .data\n" +
                            "\treturnMsg:\tdb\t\"El resultado es: %d\", 10 , 0\n");
            textSection = new StringBuilder("section .text\n" +
                            "_main:\n");
        }
    }

    public void startVarDec(String type, String priv, Symbol s) {
        //Tal vez no siempre hay que usar registros semánticos
        SemanticRegister sr_varDec = new SemanticRegister(RegisterTypes.RS_VarDec);
        stack.push(sr_varDec);
        PVariableSymbol newSymbol = new PVariableSymbol();
        newSymbol.setType(type);
        if(priv != null)
            newSymbol.setAccess(priv);
        newSymbol.setCupSymbol(s);
        table.setCurrentSymbol(newSymbol);
    }

    public void endVarDec(String id) {
        if(table.symbolExists(id)){
            reportSemanticError(id, "Symbol already declared: ");
            table.discardCurrentSymbol();
        } else{
            PVariableSymbol currentSym = ((PVariableSymbol) table.getCurrentSymbol());
            currentSym.setId(id);
            bssSection.append("\t" + table.getCurrentSymbol().getId() + ":\tresw\t2\n");
            table.addCurrenSymbol();
            if(isTopOfType(RegisterTypes.RS_DO)) {
                currentSym.setHasBeenInitialized(true);
                SemanticRegister res = stack.pop();

                SemanticRegister idReg = new SemanticRegister(RegisterTypes.RS_DO);
                idReg.setProperty("value", id);
                stack.push(idReg);

                SemanticRegister ass = new SemanticRegister(RegisterTypes.RS_DO);
                ass.setProperty("value", "=");
                stack.push(ass);

                stack.push(res);
                endVariableAssignment();
            }
            if(getRegister(RegisterTypes.RS_StructDec) != null){
                currentSym.setStructValue(true);
                SemanticRegister structReg = getRegister(RegisterTypes.RS_StructDec);
                ((PStructSymbol) structReg.getProperty("Symbol")).addProperty(currentSym);
            }
            stack.pop(); //Pop RS_VarDec
        }
    }

    public void declareStruct(String id, Symbol cupSym){
        SemanticRegister newReg = new SemanticRegister(RegisterTypes.RS_StructDec);
        PStructSymbol structSym = new PStructSymbol();
        structSym.setId(id);
        structSym.setCupSymbol(cupSym);
        structSym.setAccess("public");
        newReg.setProperty("Symbol", structSym);
        stack.push(newReg);
    }

    public void finishStruct(){
        SemanticRegister strucReg = stack.pop();
        table.addSymbol((PStructSymbol) strucReg.getProperty("Symbol"));
    }

    public void declareEnum(String id, Symbol cupSymb){
        SemanticRegister newReg = new SemanticRegister(RegisterTypes.RS_EnumDec);
        PEnumSymbol newSymbol = new PEnumSymbol();
        newSymbol.setId(id); newSymbol.setCupSymbol(cupSymb); newSymbol.setAccess("public");
        newReg.setProperty("Symbol", newSymbol);
        stack.push(newReg);
    }

    public void addEnumMember(String id, Symbol cupSymbol){
        PVariableSymbol newSymbol = new PVariableSymbol();
        newSymbol.setId(id); newSymbol.setCupSymbol(cupSymbol);
        newSymbol.setEnumValue(true);
        SemanticRegister newReg = new SemanticRegister(RegisterTypes.RS_EnumMemberDec);
        newReg.setProperty("Symbol", newSymbol);
        stack.push(newReg);
    }

    public void finishEnumDeclaration(){
        PEnumSymbol enumSymbol = ((PEnumSymbol)getRegister(RegisterTypes.RS_EnumDec).getProperty("Symbol"));
        while(stack.peek().getRegisterType() == RegisterTypes.RS_EnumMemberDec){
            PVariableSymbol member = ((PVariableSymbol) stack.pop().getProperty("Symbol"));
            enumSymbol.addMember(member);
        }
        table.addSymbol(enumSymbol);
        stack.pop();
    }

    public void startFunction(String id, Symbol s){
        if(table.symbolExists(id)){
            reportSemanticError(id,"Symbol already defined:");
            return;
        }
        SemanticRegister newRegister = new SemanticRegister(RegisterTypes.RS_FunDec);
        newRegister.setProperty("FunName", id);
        stack.push(newRegister);
        PFunctionSymbol symbol = new PFunctionSymbol();
        symbol.setId(id);
        symbol.setCupSymbol(s);
        table.setCurrentSymbol(symbol);
    }

    public void addFunParam(String type, String id, Symbol sym){
        boolean isReturnvalue = false;
        for(SemanticRegister r : stack){
            if(r.getRegisterType() == RegisterTypes.RS_RetValDec){
                isReturnvalue = true;
                break;
            }
        }
        SemanticRegister newReg = new SemanticRegister(RegisterTypes.RS_ParamDec);
        PVariableSymbol param = new PVariableSymbol();
        param.setCupSymbol(sym);
        param.setId(id);
        param.setType(type);
        param.setValue(0); //default value for parameters
        param.setParameter(!isReturnvalue);
        param.setReturnValue(isReturnvalue);
        newReg.setProperty("VarSymbol", param);
        stack.push(newReg);
        table.addSymbol(param);
    }

    public void setReturnParams(){
        //as syntactically the return values are declared the same as variables
        //this register acts as a separator
        getRegister(RegisterTypes.RS_FunDec).setProperty("Returns", true);
        SemanticRegister newReg = new SemanticRegister(RegisterTypes.RS_RetValDec);
        stack.push(newReg);
    }

    public void declareFunction(){
        PFunctionSymbol fsym = (PFunctionSymbol) table.getCurrentSymbol();

        List<PVariableSymbol> retVals = new ArrayList<>();
        if(getRegister(RegisterTypes.RS_FunDec).getProperty("Returns") != null){
            while(stack.peek().getRegisterType() == RegisterTypes.RS_ParamDec){
                SemanticRegister reg = stack.pop();
                retVals.add((PVariableSymbol) reg.getProperty("VarSymbol"));
            }
            //this is a separator that we should pop out
            stack.pop();
        }
        fsym.setReturnValues(retVals);

        StringBuilder accessValues= new StringBuilder("");
        while(stack.peek().getRegisterType() == RegisterTypes.RS_ModifierDec){
            SemanticRegister reg = stack.pop();
            accessValues.append(((String) reg.getProperty("Modifier")) + " ");
        }
        fsym.setAccess(accessValues.length() != 0 ? accessValues.toString() : null);

        List<PVariableSymbol> params = new ArrayList<>();
        while(stack.peek().getRegisterType() == RegisterTypes.RS_ParamDec){
            SemanticRegister reg = stack.pop();
            params.add((PVariableSymbol) reg.getProperty("VarSymbol"));
        }
        fsym.setParameters(params);
        table.addCurrenSymbol();
    }

    public void addFunModifier(String m){
        System.out.print("Esto es: ");
        System.out.println(m);
        SemanticRegister newRegister = new SemanticRegister(RegisterTypes.RS_ModifierDec);
        newRegister.setProperty("Modifier", m);
    }

    public void endFunction(){
        SemanticRegister funReg = stack.pop(); //pop RS_FunDeclaration
        if(funReg.getProperty("Returns") != null && funReg.getProperty("HasRetStm") == null){
            reportSemanticError("'"+((String)funReg.getProperty("FunName"))+"'", "This function has no return statement: ");
        }
    }

    public void rememberConstant(Object constant){
        SemanticRegister constReg = new SemanticRegister(RegisterTypes.RS_DO);
        constReg.setProperty("type", "const");
        constReg.setProperty("value", constant);
        this.stack.push(constReg);
    }

    public void rememberOperator(Object operator){
        SemanticRegister opReg = new SemanticRegister(RegisterTypes.RS_DO);
        opReg.setProperty("value", operator);
        this.stack.push(opReg);
    }

    public void rememberVariable(Object variable) {
        SemanticRegister variableReg = new SemanticRegister(RegisterTypes.RS_DO);
        variableReg.setProperty("type", "variable");
        variableReg.setProperty("value", variable);
        this.stack.push(variableReg);
    }

    public void startVariableAssignment(Object variable, Object operator){
        rememberVariable(variable);
        rememberOperator(operator);
        this.textSection.append("\tmov dword [return], 0" + "\n");
    }

    public void endVariableAssignment(){
        SemanticRegister val = this.stack.pop();
        SemanticRegister assign = this.stack.pop();
        SemanticRegister ident = this.stack.pop();
        if(!table.symbolExists((String)ident.getProperty("value"))){
            reportSemanticError("'"+ident.getProperty("value")+"'", "Symbol not defined: ");
            return;
        }

        PVariableSymbol var = ((PVariableSymbol) table.getSymbol(((String) ident.getProperty("value"))));
        if(!var.hasBeenInitialized() && !assign.getProperty("value").equals("=")){
            reportSemanticError("'"+var.getId()+"'", "You can't increment nor decrement an uninitialized variable: ");
            return;
        }
        if(!var.hasBeenInitialized()){
            var.setHasBeenInitialized(true);
        }

        if (assign.getProperty("value").equals("=")){
            textSection.append("\t;assign " + ident.getProperty("value") + " = " + val.getProperty("value") + "\n");
            if (val.getProperty("type").equals("const")){
                textSection.append("\tmov dword [" + ident.getProperty("value") + "], " + val.getProperty("value") + "\n");
            }
            else if (val.getProperty("type").equals("variable")){
                textSection.append(
                        "\tPUSH eax" + "\n" +
                        "\tMOV eax, dword [" + val.getProperty("value") + "]\n" +
                        "\tMOV dword [" + ident.getProperty("value") + "], " + "eax" + "\n" +
                        "\tPOP eax\n"
                );
            }
        }
        else if (assign.getProperty("value").equals("+=")){
            textSection.append("\t;assign " + ident.getProperty("value") + " += " + val.getProperty("value") + "\n");
            if (val.getProperty("type").equals("const")){
                textSection.append("\tadd dword [" + ident.getProperty("value") + "], " + val.getProperty("value") + "\n");
            }
            else if (val.getProperty("type").equals("variable")){
                textSection.append(
                        "\tPUSH eax" + "\n" +
                        "\tMOV eax, dword [" + val.getProperty("value") + "]\n" +
                        "\tADD dword [" + ident.getProperty("value") + "], " + "eax" + "\n" +
                        "\tPOP eax\n"
                );
            }
        }
        else if (assign.getProperty("value").equals("-=")){
            textSection.append("\t;assign " + ident.getProperty("value") + " -= " + val.getProperty("value") + "\n");
            if (val.getProperty("type").equals("const")){
                textSection.append("\tsub dword [" + ident.getProperty("value") + "], " + val.getProperty("value") + "\n");
            }
            else if (val.getProperty("type").equals("variable")){
                textSection.append(
                        "\tPUSH eax" + "\n" +
                        "\tMOV eax, dword [" + val.getProperty("value") + "]\n" +
                        "\tSUB dword [" + ident.getProperty("value") + "], " + "eax" + "\n" +
                        "\tPOP eax\n");
            }
        }
    }

    public void evalNumericBinary(){
        SemanticRegister resultReg = new SemanticRegister(RegisterTypes.RS_DO);
        SemanticRegister r2 = this.stack.pop();
        SemanticRegister op = this.stack.pop();
        SemanticRegister r1 = this.stack.pop();
        if (r1.getProperty("type").equals("const") && r2.getProperty("type").equals("const")) {
            resultReg.setProperty("type","const");
            if (op.getProperty("value").equals("+")){
                resultReg.setProperty("value", (Integer) r1.getProperty("value") + (Integer) r2.getProperty("value"));
            }
            else if (op.getProperty("value").equals("-")){
                resultReg.setProperty("value", (Integer) r1.getProperty("value") - (Integer) r2.getProperty("value"));
            }
        }
        else{
            resultReg.setProperty("type","variable");
            resultReg.setProperty("value", "return");
            if(op.getProperty("value") == null){
                System.out.print("op:  ");
                System.out.println(op);
                System.out.println(op.getRegisterType());
            }
            if (op.getProperty("value").equals("+")){
                if (r1.getProperty("type").equals("variable") && r2.getProperty("type").equals("variable")){
                    textSection.append(
                                    "\t;add return = "+r1.getProperty("value")+" + "+r2.getProperty("value")+" \n"+
                                    "\tPUSH eax" + "\n" +
                                    "\tPUSH ebx" + "\n" +
                                    "\tMOV eax, dword [" + r1.getProperty("value") + "]\n" +
                                    "\tMOV ebx, dword [" + r2.getProperty("value") + "]\n" +
                                    "\tADD eax, ebx" + "\n" +
                                    "\tMOV dword [return], eax" + "\n" +
                                    "\tPOP ebx" + "\n" +
                                    "\tPOP eax\n"
                    );
                }
                else if (r1.getProperty("type").equals("variable")){
                    textSection.append(
                                    "\t;add return = "+r1.getProperty("value")+" + "+r2.getProperty("value")+" \n"+
                                    "\tPUSH eax" + "\n" +
                                    "\tPUSH ebx" + "\n" +
                                    "\tMOV eax, dword [" + r1.getProperty("value") + "]\n" +
                                    "\tMOV ebx, " + r2.getProperty("value") + "\n" +
                                    "\tADD eax, ebx" + "\n" +
                                    "\tMOV dword [return], eax" + "\n" +
                                    "\tPOP ebx" + "\n" +
                                    "\tPOP eax\n"
                    );
                }
                else if (r2.getProperty("type").equals("variable")){
                    textSection.append(
                                    "\t;add return = "+r1.getProperty("value")+" + "+r2.getProperty("value")+" \n"+
                                    "\tPUSH eax" + "\n" +
                                    "\tPUSH ebx" + "\n" +
                                    "\tMOV eax, " + r1.getProperty("value") + "\n" +
                                    "\tMOV ebx, dword [" + r2.getProperty("value") + "]\n" +
                                    "\tADD eax, ebx" + "\n" +
                                    "\tMOV dword [return], eax" + "\n" +
                                    "\tPOP ebx" + "\n" +
                                    "\tPOP eax\n"
                    );
                }
            }
            else if (op.getProperty("value").equals("-")){
                if (r1.getProperty("type").equals("variable") && r2.getProperty("type").equals("variable")){
                    textSection.append(
                                    "\t;subtract return = "+r1.getProperty("value")+" - "+r2.getProperty("value")+" \n"+
                                    "\tPUSH eax" + "\n" +
                                    "\tPUSH ebx" + "\n" +
                                    "\tMOV eax, dword [" + r1.getProperty("value") + "]\n" +
                                    "\tMOV ebx, dword [" + r2.getProperty("value") + "]\n" +
                                    "\tSUB eax, ebx" + "\n" +
                                    "\tMOV dword [return], eax" + "\n" +
                                    "\tPOP ebx" + "\n" +
                                    "\tPOP eax\n"
                    );
                }
                else if (r1.getProperty("type").equals("variable")){
                    textSection.append(
                                    "\t;subtract return = "+r1.getProperty("value")+" - "+r2.getProperty("value")+" \n"+
                                    "\tPUSH eax" + "\n" +
                                    "\tPUSH ebx" + "\n" +
                                    "\tMOV eax, dword [" + r1.getProperty("value") + "]\n" +
                                    "\tMOV ebx, " + r2.getProperty("value") + "\n" +
                                    "\tSUB eax, ebx" + "\n" +
                                    "\tMOV dword [return], eax" + "\n" +
                                    "\tPOP ebx" + "\n" +
                                    "\tPOP eax\n"
                    );
                }
                else if (r2.getProperty("type").equals("variable")){
                    textSection.append(
                                    "\t;add return = "+r1.getProperty("value")+" + "+r2.getProperty("value")+" \n"+
                                    "\tPUSH eax" + "\n" +
                                    "\tPUSH ebx" + "\n" +
                                    "\tMOV eax, " + r1.getProperty("value") + "\n" +
                                    "\tMOV ebx, dword [" + r2.getProperty("value") + "]\n" +
                                    "\tADD eax, ebx" + "\n" +
                                    "\tMOV dword [return], eax" + "\n" +
                                    "\tPOP ebx" + "\n" +
                                    "\tPOP eax\n"
                    );
                }
            }
        }
        this.stack.push(resultReg);
    }

    public void evalBooleanBinary(String label){
        SemanticRegister r2 = this.stack.pop();
        SemanticRegister op = this.stack.pop();
        SemanticRegister r1 = this.stack.pop();

        String strR1 = r1.getProperty("type").equals("const") ? r1.getProperty("value")+"" : "dword ["+r1.getProperty("value") + "]";
        String strR2 = r2.getProperty("type").equals("const") ? r2.getProperty("value")+"" : "dword ["+r2.getProperty("value") + "]";

        textSection.append(
                        "\tPUSH eax" + "\n"+
                        "\tPUSH ebx" + "\n"+
                        "\tMOV eax, " + strR1 + "\n" +
                        "\tMOV ebx, " + strR2 + "\n" +
                        "\tCMP eax, ebx" + "\n"+
                        "\tPOP ebx" + "\n"+
                        "\tPOP eax" + "\n"
        );

        if (op.getProperty("value").equals("==")){
            textSection.append(
                            "\tJE " + label + "\n"
            );
        }
        if (op.getProperty("value").equals("!=")){
            textSection.append(
                            "\tJNE " + label + "\n"
            );
        }
        if (op.getProperty("value").equals(">")){
            textSection.append(
                            "\tJG "+ label + "\n"
            );
        }
        if (op.getProperty("value").equals(">=")){
            textSection.append(
                            "\tJGE " + label + "\n"
            );
        }
        if (op.getProperty("value").equals("<=")){
            textSection.append(
                            "\tJLE "+ label + "\n"
            );
        }
        if (op.getProperty("value").equals("<")){
            textSection.append(
                            "\tJl "+ label + "\n"

            );
        }
    }

    public void generateReturn() {
        SemanticRegister funcReg = getRegister(RegisterTypes.RS_FunDec);
        if(stack.peek().getRegisterType() != RegisterTypes.RS_DO){
            if(funcReg.getProperty("Returns") != null){
                reportSemanticError(funcReg.getProperty("FunName") + "'", "Must return a value in function: '");
                funcReg.setProperty("HasRetStm", true);
            } else {
                textSection.append("\t; stop the program\n");
                textSection.append("\tMOV eax,1            ; The system call for exit (sys_exit)\n" +
                        "    MOV ebx,0            ; Exit with return code of 0 (no error)\n" +
                        "    INT 80h;");
            }
        } else {
            SemanticRegister resultRes = stack.pop();
            if(funcReg.getProperty("Returns") != null){
                funcReg.setProperty("HasRetStm", true);
                String result = ((String)resultRes.getProperty("type")).equals("const")
                        ? resultRes.getProperty("value").toString() : "["+resultRes.getProperty("value")+"]" ;
                textSection.append("\t; return " + result + "\n");
                if(isElf32){
                    textSection.append("\tPUSH  dword "+ result +"\n" +
                            "\tPUSH  returnMsg\n" +
                            "\tCALL  printf\n" +
                            "\tMOV eax,1            ; The system call for exit (sys_exit)\n" +
                            "    MOV ebx,0            ; Exit with return code of 0 (no error)\n" +
                            "    INT 80h;");
                }else {
                    textSection.append("\tPUSH  dword "+ result +"\n" +
                            "\tPUSH  returnMsg\n" +
                            "\tCALL  _printf\n" +
                            "\tMOV eax,1            ; The system call for exit (sys_exit)\n" +
                            "    MOV ebx,0            ; Exit with return code of 0 (no error)\n" +
                            "    INT 80h;");
                }
            } else {
                reportSemanticError((String)funcReg.getProperty("FunName"), "Can't return a value in function: ");
            }

        }
    }

    public void startif(){
        SemanticRegister sr = new SemanticRegister(RegisterTypes.IF_VarDec);
        this.ifCounter+=1;
        sr.setProperty("elseIf","elseIF"+this.ifCounter);
        sr.setProperty("endIf","endIF"+this.ifCounter);
        sr.setProperty("startIF","startIF"+this.ifCounter);

        this.stack.push(sr);


    }

    public void testif(){
        Object[] pila = this.stack.toArray();
        String b="";
        String c ="";
        for (int i = pila.length-1; i>0 ;i--) {
            if (((SemanticRegister) pila[i]).getRegisterType() == RegisterTypes.IF_VarDec) {
                b = (String) (((SemanticRegister) pila[i]).getProperty("elseIf"));
                c = (String) (((SemanticRegister) pila[i]).getProperty("startIF"));
            }
        }
            this.evalBooleanBinary(c);

            textSection.append("\tJMP " + b + '\n');
            textSection.append( c + ":" + '\n');
    }

    public void startElse(){
        //Generar "JUMP"+RS_IF.eti_label

        //Generar RS_IF.ele_label +
        SemanticRegister a=this.stack.lastElement();

        String e = (String)(a.getProperty("endIf"));
        String b = (String)(a.getProperty("elseIf")) + ':';
        textSection.append("\tJMP "+e + '\n');
        textSection.append(b+'\n');

    }

    public void endif(){
        //Generar RS_IF.exit_label
        //POP RS_IF
        SemanticRegister a=this.stack.pop();
        String b = (String)(a.getProperty("endIf")) + ':';
        textSection.append(b+'\n');
    }

    public void startwhile(){
        SemanticRegister sr = new SemanticRegister(RegisterTypes.WHILE_VarDec);
        this.whileCounter+=1;
        sr.setProperty("startwhile","startwhile"+this.whileCounter);
        sr.setProperty("whilestm","whilestm"+this.whileCounter);
        sr.setProperty("endwhile","endwhile"+this.whileCounter);
        this.stack.push(sr);
    }

    public void testwhile(){
        Object[] pila = this.stack.toArray();
        String b="";
        String c ="";
        String e ="";
        for (int i = pila.length-1; i>0 ;i--) {
            if (((SemanticRegister) pila[i]).getRegisterType() == RegisterTypes.WHILE_VarDec) {
                b = (String) (((SemanticRegister) pila[i]).getProperty("whilestm"));
                c = (String) (((SemanticRegister) pila[i]).getProperty("startwhile"));
                e = (String) (((SemanticRegister) pila[i]).getProperty("endwhile"));
            }
        }
        textSection.append(c+":"+'\n');
        this.evalBooleanBinary(b);
        textSection.append("\t JMP "+e + '\n');
        textSection.append(b+":"+'\n');

    }

    public void endwhile(){
        SemanticRegister a=this.stack.pop();
        textSection.append("\t JMP "+(a.getProperty("startwhile")) + '\n');
        String b = (String)(a.getProperty("endwhile")) + ':';
        textSection.append(b+'\n');

    }

    public void continuestm(){
        Object[] pila = this.stack.toArray();
        boolean hasBeenFound =false;
        for (int i = pila.length-1;i>0;i--){
            SemanticRegister sr= (SemanticRegister)pila[i];
            if(sr.getRegisterType()==RegisterTypes.WHILE_VarDec){
                textSection.append("\t JMP "+sr.getProperty("startwhile") + '\n');
                hasBeenFound = true;
                break;
            }
        }
        if(hasBeenFound==false){
            this.reportSemanticError("continue;", "This lexeme is out of a cycle:");

        }
    }

    public void breakstm(){
        boolean hasBeenFound =false;
        Object[] pila = this.stack.toArray();
        for (int i = pila.length-1;i>0;i--){
            SemanticRegister sr= (SemanticRegister)pila[i];
            if(sr.getRegisterType()==RegisterTypes.WHILE_VarDec){
                textSection.append("\t JMP "+sr.getProperty("endwhile") + '\n');
                hasBeenFound = true;
                break;
            }
        }
        if(hasBeenFound==false){
            this.reportSemanticError("break;", "This lexeme is out of a cycle:");

        }
    }

    public boolean isTopOfType(int type){
        return stack.peek().getRegisterType() == type;
    }

    public SemanticRegister getRegister(int type){
        for (int i = 0; i < stack.size(); i++) {
            SemanticRegister reg = stack.elementAt(stack.size() -1 -i);
            if(reg.getRegisterType() == type){
                return reg;
            }
        }
        return null;
    }

    public void finishCodeGeneration(){
        if(hasError)
            return;
        try {
            fileContent.append(dataSection.toString());
            fileContent.append(bssSection.toString());
            fileContent.append(textSection.toString());
            FileWriter writer = new FileWriter(outputName);
            writer.write(fileContent.toString());
            writer.close();
        } catch (IOException e) {
            System.err.println("Hubo un error al crear el archivo.");
            e.printStackTrace();
        }
    }

    public Object identExp(Object r){
        if(table.symbolExists((String)r)){
            PSymbol sym = table.getSymbol((String)r);
            //si es una variable y no se ha modificado su valor
            if(sym instanceof PVariableSymbol){
                if(((PVariableSymbol) sym).hasBeenInitialized()){
                    return sym.getId();
                } else {
                    reportSemanticError("'"+r+"'","Var not initialized: ");
                }
            } else {
                reportSemanticError("'"+r+"'", "Function calls not supported: ");
            }
        } else {
            reportSemanticError("'" + r + "'", "Symbol not defined: ");
        }
        return 0;
    }

    public void reportSemanticError(String message, String info){
        parser.report_error(message, "Semantic Error: " + info);
        abortCodeGeneration();
    }

    public void abortCodeGeneration(){ //called when an error ocurred
        if(!hasError){
            System.err.println("Code Generator: Aborting code generation!!!\n");
            this.hasError = true;
        }
    }

    public void testStack() {
        if(hasError)
            return;
        //simular algún evento particular
    }

}

package com.company.SemanticStack;

public interface RegisterTypes {
    static final int RS_VarDec = 501;
    static final int RS_FunDec = 254;
    static final int RS_ParamDec = 992;
    static final int RS_RetValDec = 404;
    static final int RS_ModifierDec = 242;
    static final int RS_StructDec = 315;
    static final int RS_EnumDec = 86;
    static final int RS_EnumMemberDec = 135;
    static final int RS_DO = 777;
    static final int IF_VarDec = 911;
    static final int WHILE_VarDec = 666;
}

package com.company.PClasses;

import java_cup.runtime.Symbol;

public class PSymbol {
    private String id;
    private Symbol cupSymbol;
    private String access = "public"; //can be private public or local by defect

    public PSymbol(String id, String access, Symbol symbol){
        this.id = id;
        this.cupSymbol = symbol;
        this.access = access;
    }

    public PSymbol(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Symbol getCupSymbol() {
        return cupSymbol;
    }

    public void setCupSymbol(Symbol cupSymbol) {
        this.cupSymbol = cupSymbol;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public String toString(){
        return access + " symbol: " + id ;
    }

    public Object getValue(){
        return cupSymbol.value;
    }

    public void setValue(Object value) {
        this.cupSymbol.value = value;
    }
}



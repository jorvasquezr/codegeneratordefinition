package com.company.PClasses;

import java.util.ArrayList;
import java.util.List;

public class PStructSymbol extends PSymbol {
    List<PVariableSymbol> properties = new ArrayList<>();

    public PStructSymbol(){ super(); }

    public List<PVariableSymbol> getProperties() {
        return properties;
    }

    public void addProperty(PVariableSymbol sym ){
        properties.add(sym);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("");
        for(PVariableSymbol elem : properties){
            builder.append("\t\t" + elem + "\n");
        }
        return "Struct '"+getId()+"':\n" +
                "\tProperties:[" + (builder.length() == 0 ?  "]" :"\n"+(builder.toString() + "\t]")) + "\n";
    }
}

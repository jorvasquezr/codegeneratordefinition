package com.company.PClasses;

import java_cup.runtime.Symbol;

import java.util.HashMap;

public class PSymbolTable {
    private HashMap<String, PSymbol> table;
    private PSymbol currentSymbol;

    public PSymbolTable() {
        table = new HashMap<>();
    }

    public void addSymbol(PSymbol symbol){
        table.put(symbol.getId(), symbol);
    }

    public PSymbol getSymbol(String id){
        return table.getOrDefault(id, null);
    }

    public String getSymbolType(String id){
        if(table.containsKey(id) && table.get(id) instanceof PVariableSymbol)
            return ((PVariableSymbol) table.get(id)).getType();
        else
            return null;
    }

    public boolean symbolExists(String id){
        return table.containsKey(id);
    }

    public void printSymbols(){
        for(PSymbol sym : table.values()){
            if(sym instanceof PVariableSymbol && (((PVariableSymbol) sym).isReturnValue()) | ((PVariableSymbol) sym).isParameter()
                                                | ((PVariableSymbol) sym).isEnumValue() | ((PVariableSymbol) sym).isStructValue()){
                continue;
            }
            System.out.println(sym.toString());
        }
    }

    public void setCurrentSymbol(PSymbol currentSymbol) {
        this.currentSymbol = currentSymbol;
    }

    public PSymbol getCurrentSymbol(){
        return this.currentSymbol;
    }

    public void setCurrentSymbolsCupSym(Symbol sym){
        this.currentSymbol.setCupSymbol(sym);
    }

    public void addCurrenSymbol(){
        addSymbol(currentSymbol);
        currentSymbol = null;
    }

    public void discardCurrentSymbol(){
        currentSymbol = null;
    }
}

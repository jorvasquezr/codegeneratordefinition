package com.company.PClasses;

import java_cup.runtime.Symbol;

import java.util.List;

public class PFunctionSymbol extends PSymbol {

    private List<PVariableSymbol> parameters;
    private List<PVariableSymbol> returnValues;

    public PFunctionSymbol(String id, Symbol symbol, String access, List<PVariableSymbol> parameters, List<PVariableSymbol> returnValues) {
        super(id, access, symbol);
        this.parameters = parameters;
        this.returnValues = returnValues;
    }

    public PFunctionSymbol(){
        super();
    }

    public List<PVariableSymbol> getParameters() {
        return parameters;
    }

    public void setParameters(List<PVariableSymbol> parameters) {
        this.parameters = parameters;
    }

    public List<PVariableSymbol> getReturnValues() {
        return returnValues;
    }

    public void setReturnValues(List<PVariableSymbol> returnValues) {
        this.returnValues = returnValues;
    }

    @Override
    public String toString() {
        StringBuilder params = new StringBuilder("");
        StringBuilder returnValues = new StringBuilder("");

        for(PVariableSymbol param : this.parameters) params.append("\n\t\t" + param);
        for(PVariableSymbol retVal : this.returnValues) returnValues.append("\n\t\t" + retVal);

        if(params.length() != 0) params.append("\n\t");
        if(returnValues.length() != 0) returnValues.append("\n\t");

        return "Function '"+ getId() +"':\n\t" +
                    "Parameters: [" + params + "]\n\t" +
                    "Return Values: [" + returnValues + "]\n";
    }
}

package com.company.PClasses;

public interface PType {
    String INT = "int";
    String UINT = "uint";
    String STRING = "string";
    String BOOL = "bool";
    String BYTE = "byte";
    String FIXED = "fixed";
    String ADDRESS = "address";
    String VAR = "var";
    String UFIXED = "ufixed";
    int DEFAULT_SIZE = 4;
}

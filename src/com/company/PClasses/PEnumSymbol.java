package com.company.PClasses;

import java.util.ArrayList;
import java.util.List;

public class PEnumSymbol extends PSymbol {

    private List<PVariableSymbol> members = new ArrayList<>();

    public PEnumSymbol(){ super(); }

    public List<PVariableSymbol> getMembers() {
        return members;
    }

    public void addMember(PVariableSymbol member){
        members.add(member);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("");
        for(PVariableSymbol elem : members){
            builder.append("\t\t" + elem + "\n");
        }
        return "Struct '"+getId()+"':\n" +
                "\tMembers:[" + (builder.length() == 0 ?  "]" :"\n"+(builder.toString() + "\t]")) + "\n";
    }
}

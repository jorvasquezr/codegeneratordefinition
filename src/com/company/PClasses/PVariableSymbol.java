package com.company.PClasses;

import java_cup.runtime.Symbol;

public class PVariableSymbol extends PSymbol{

    private String type; //idea is to use PType
    private Object value;
    private int size = PType.DEFAULT_SIZE;
    private boolean isParameter = false;
    private boolean isReturnValue = false;
    private boolean isStructValue = false;
    private boolean isEnumValue = false;
    private boolean hasBeenInitialized = false;

    public PVariableSymbol(){}

    public PVariableSymbol(String id, String access, Symbol symbol, String type, Object value) {
        super(id, access, symbol);
        this.type = type;
        this.value = value;
    }

    public PVariableSymbol(String id, Symbol symbol, String access, String type, int size, Object value) {
        super(id, access, symbol);
        this.type = type;
        this.value = value;
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public int getDeclarationLine() {
        return this.getCupSymbol().left;
    }

    public int getDeclarationColumn() {
        return this.getCupSymbol().right;
    }

    public boolean hasBeenInitialized() {
        return hasBeenInitialized;
    }

    public void setHasBeenInitialized(boolean hasBeenInitialized) {
        this.hasBeenInitialized = hasBeenInitialized;
    }

    public boolean isParameter() {
        return isParameter;
    }

    public void setParameter(boolean parameter) {
        isParameter = parameter;
    }

    public boolean isReturnValue() {
        return isReturnValue;
    }

    public void setReturnValue(boolean returnValue) {
        isReturnValue = returnValue;
    }

    public boolean isStructValue() {
        return isStructValue;
    }

    public void setStructValue(boolean structValue) {
        isStructValue = structValue;
    }

    public boolean isEnumValue() {
        return isEnumValue;
    }

    public void setEnumValue(boolean enumValue) {
        isEnumValue = enumValue;
    }

    @Override
    public String toString() {
        if(isReturnValue | isParameter){
            return (isParameter ? "Parameter: '" : "Return Value: '") + getId() + "' of type "+ getType() + " and size: " + size ;
        } else if (isEnumValue) {
            return "Enum member: '" +getId()+ "'";
        }
        return "Variable: " + getAccess() + " '" + getId() + "' of type "+ getType() + " and size: " + size ;
    }
}

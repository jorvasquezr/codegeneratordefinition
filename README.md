### Instituto Tecnológico de Costa Rica
### Escuela de Ingeniería en Computación 
### Compiladores e Intérpretes

### Profesora: Erika Marín Schumann
  
### Tarea Programada: Generador de Código

### Estudiantes:
-  Gerardo López: 2018168237
-  David Salazar: 2018100227
-  Arturo Vásquez:2018102354

* * *
### Consideraciones generales:

#### Librerías necesarias:
```
java-cup-11a.jar
java-cup-11b-runtime.jar
jflex-1.6.1.jar
```
#### Comandos de compilación del lexer.jflex y parser.cup

```
java -jar jflex-1.6.1.jar lexer.jflex
java -jar java-cup-11a.jar parser.cup
```

#### Se usa como lenguaje objetivo el ensamblador NASM con en linker de GCC
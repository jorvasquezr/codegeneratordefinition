global _main 
extern _printf
section .data
	returnMsg:	db	"El resultado es: %d", 10 , 0
section .bss
	return:	resw	2
 	prueba:	resw	2
	prueba2:	resw	2
	prueba3:	resw	2
section .text
_main:
	mov dword [return], 0
	;subtract return = prueba - prueba2 
	push eax
	push ebx
	mov eax, dword [prueba]
	mov ebx, dword [prueba2]
	sub eax, ebx
	mov dword [return], eax
	pop ebx
	pop eax
	;add return = return + 283 
	push eax
	push ebx
	mov eax, dword [return]
	mov ebx, 283
	add eax, ebx
	mov dword [return], eax
	pop ebx
	pop eax
	;assign prueba3 = return
	push eax
	mov eax, dword [return]
	mov dword [prueba3], eax
	pop eax
	; return [prueba3]
	push  dword [prueba3]
	push  returnMsg
	call  _printf
	mov eax,1            ; The system call for exit (sys_exit)
    mov ebx,0            ; Exit with return code of 0 (no error)
    int 80h;